import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import API from '../../../services/Api';

class EditCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customer: {},
            loading: true,
            error: false,
        };
    }

    componentDidMount() {
        const { history: { replace }, match: { params: { id } }, token } = this.props;
        API.get(`customers/search/${id}`, token)
            .then((res) => {
                if (res.error) {
                    return replace('/login');
                }
                this.setState({ loading: false, customer: res.customer })
            })
            .catch(() => this.setState({ loading: false, error: true }));
    }

    changeField = field => (e) => {
        let { customer } = this.state;
        customer[field] = e.target.value;
        this.setState({ customer });
    };

    save = () => {
        const { customer } = this.state;
        const { history: { goBack }, token } = this.props;
        this.setState({ loading: true });
        API.put('customers', { id: customer._id, changeValues: customer }, token)
            .then(() => goBack())
            .catch(() => goBack());
    }


    render() {
        const { customer, loading, error } = this.state;
        return (
            <div>
                EDIT
                { loading && <span>Loading</span> }
                { error && <span>Error while getting customer info</span> }
                {
                    customer && Object.keys(customer).length > 0 && (
                        <div>
                            <div>
                                <label>Id</label>
                                <input type="text" onChange={e => this.changeField('id')(e)} value={customer.id} />
                            </div>
                            <div>
                                <label>Name</label>
                                <input type="text" onChange={e => this.changeField('name')(e)} value={customer.name} />
                            </div>
                            <div>
                                <label>Surname</label>
                                <input type="text" onChange={e => this.changeField('surname')(e)} value={customer.surname} />
                            </div>
                            <div>
                                <label>Photo</label>
                                <input type="text" onChange={e => this.changeField('photo')(e)} value={customer.photo} />
                            </div>
                            <div>
                                <button onClick={this.save}>Save</button>
                                <Link to="/customers">Back</Link>
                            </div>
                        </div>
                    )
                }
            </div>
        );
    }
}

function mapStateToProps({ userReducer: { token } }) {
    return {
        token,
    };
}

export default connect(mapStateToProps, null)(EditCustomer);