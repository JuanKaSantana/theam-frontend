import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import c from 'classnames';

import API from '../../services/Api';
import { invalidateList as invalidateCustomerList } from '../../redux/actions/customerAction';
import { invalidateList as invalidateUserList } from '../../redux/actions/userActions';

const ListElement = ({ singleData, from, invalidateCustomerList, invalidateUserList, token }) => {
    const onClick = (from, id, invalidateCustomerList, invalidateUserList, token) => {
        const secure = window.confirm('Are you sure that you want to delete this user?');
        if (secure) {
            API.delete(`${from.substring(1)}/${id}`, token)
                .then(() => {
                    if (from.includes('customer')) {
                        invalidateCustomerList();
                    } else {
                        invalidateUserList();
                    }
                });
        }
    };
    return (
        <tr className={c('tr')}>
            {
                Object.keys(singleData).map((key) => {
                    if (key === 'admin' ) {
                        return <td className={c('td')}>{singleData[key].toString()}</td>;
                    } else if (key !== '_id' && key !== 'password') {
                        return <td className={c('td')}>{singleData[key]}</td>;
                    } else {
                        return null;
                    }
                })
            }
            <td className={c('td')}>
                <Link to={`${from}/edit/${singleData._id}`}>Edit</Link>
                <button onClick={() => onClick(from, singleData._id, invalidateCustomerList, invalidateUserList, token)}>Delete</button>
            </td>
        </tr>
    );
};

function mapStateToProps({ userReducer: { token } }) {
    return {
        token,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        invalidateCustomerList: () => dispatch(invalidateCustomerList()),
        invalidateUserList: () => dispatch(invalidateUserList()),
    };   
}

export default connect(mapStateToProps, mapDispatchToProps)(ListElement);